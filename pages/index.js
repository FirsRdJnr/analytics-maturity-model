import Layout from "../components/Layout/Layout";
import Content from '../components/Content/Index.js';

const Index = () => (
    <Layout >
      <Content />
    </Layout>
);

export default Index