import Layout from "../components/Layout/Layout";
import Content from '../components/Content/Legals/Terms.js';

export default () => (
    <Layout >
      <Content />
    </Layout>
);
