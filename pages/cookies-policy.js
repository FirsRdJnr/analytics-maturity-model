import Layout from "../components/Layout/Layout";

export default () => (
    <Layout >
        <h3>What are Cookies?</h3>
        <p>Cookies are small files which are saved to your computer’s hard drive by websites that you visit. They are widely used to make websites work, or work better, as well as to provide information to the owners of the site. A cookie often contains a unique number, which can be used to recognise your computer when you return to a website you have previously visited.</p>
        <h3>How do we use Cookies?</h3>
        <p>We use cookies to enhance the online experience of our visitors. We also use them to understand how our website is being used, to inform the content we write and to enable us to make improvements to our website and services.</p>
        <h3>Your rights</h3>
        <p>You have the right to choose whether you want to accept these cookies. By default, your browser will accept cookies – this can be changed by amending the controls on your browser to reflect your cookie preferences. Please note, disabling cookies may prevent you from using the full range of services available on this website.</p>
        <h3>Third Party Cookies</h3>
        <p>Our website uses tracking software to monitor our visitors to understand how they use it. We use software provided by Google Analytics, which use cookies to track visitor usage. This software will save a cookie to your computer hard drive to track and monitor your engagement and use of the website, and to help identify you on future visits. It will not store, save or collect personal information.</p>
  </Layout>
);
