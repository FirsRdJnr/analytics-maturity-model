import styles from './Button.scss';

export default class Button extends React.Component {

    render() {
        return (
            <React.Fragment>
            <style jsx>{styles}</style>

                <button>
                    <span>{this.props.text}</span>
                </button>


            </React.Fragment>
        );
      }
}

