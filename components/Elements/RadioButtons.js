import styles from './RadioButtons.scss';

export default class RadioButtons extends React.Component {

    render() {
        return (
            <React.Fragment>
            <style jsx>{styles}</style>

            <article>
                <svg viewBox="0 0 283.5 283.5"><path d={this.props.svg} /></svg>

                <input
                    type="radio"
                    id={this.props.value}
                    value={this.props.value}
                    checked={this.props.checked}
                    onChange={this.props.onChange}
                />
                <label for={this.props.value}>{this.props.value}</label>
            </article>



            </React.Fragment>
        );
      }
}

