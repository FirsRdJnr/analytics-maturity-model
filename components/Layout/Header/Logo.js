import styles from './Logo.scss';

export default class Logo extends React.Component {
    render() {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>
                    <h1>Analytics Maturity Framework</h1>

                    <span>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="data-shed"
                            viewBox="0 0 200 280"
                        >
                            <path d="M178 175.4c-2.1-41.6-36.5-74.8-78.7-74.8-43.5 0-78.8 35.3-78.8 78.8s35.3 78.8 78.8 78.8 78.8-35.3 78.8-78.8l1.3-158.1" className="init" />
                            <path d="M178 175.4c-2.1-41.6-36.5-74.8-78.7-74.8-43.5 0-78.8 35.3-78.8 78.8s35.3 78.8 78.8 78.8 78.8-35.3 78.8-78.8l1.3-158.1" className="bg" />
                            <path d="M178 175.4c-2.1-41.6-36.5-74.8-78.7-74.8-43.5 0-78.8 35.3-78.8 78.8s35.3 78.8 78.8 78.8 78.8-35.3 78.8-78.8l1.3-158.1" className="seg-one" />
                            <path d="M178 175.4c-2.1-41.6-36.5-74.8-78.7-74.8-43.5 0-78.8 35.3-78.8 78.8s35.3 78.8 78.8 78.8 78.8-35.3 78.8-78.8l1.3-158.1" className="seg-two" />
                            <path d="M178 175.4c-2.1-41.6-36.5-74.8-78.7-74.8-43.5 0-78.8 35.3-78.8 78.8s35.3 78.8 78.8 78.8 78.8-35.3 78.8-78.8l1.3-158.1" className="seg-three" />
                        </svg>
                        <h1>The DataShed</h1>
                    </span>


            </React.Fragment>
        );
    }
}
