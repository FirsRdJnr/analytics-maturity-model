import Head from "next/head";
import Logo from './Logo';

import styles from './Header.scss';

export default class Header extends React.Component {
    render() {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>
                <Head>
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <link href="https://fonts.googleapis.com/css?family=Catamaran&display=swap" rel="stylesheet" />
                </Head>

                <header>
                    <section>
                        <Logo />
                    </section>
                </header>
            </React.Fragment>
        );
    }
}
