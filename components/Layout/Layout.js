import Header from './Header/Header';
import Footer from './Footer/Footer';

import styles from './Layout.scss';

const Layout = props => (
    <React.Fragment> 
        <style jsx>{styles}</style>

        <Header />

        <main>
            {props.children}
        </main>

        <Footer />
    </React.Fragment>
);

export default Layout;