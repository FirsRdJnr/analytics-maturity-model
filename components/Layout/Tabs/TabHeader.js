import styles from './TabHeader.scss';

export default class TabHeader extends React.Component {
    doClick(index) {
        this.props.click(index);
    }

    render() {
        let activeClass = this.props.activeId;

        let tabs = this.props.data.map((item, index) => {
            return (
                <React.Fragment>
                    <style jsx>{styles}</style>
                    <li className={(activeClass === index ? 'active' : '')}>
                        <a onClick={this.doClick.bind(this, index)} >
                            <span>{item.name}</span>
                        </a>
                    </li>
                </React.Fragment>
            )
        });

        return tabs
    }

}