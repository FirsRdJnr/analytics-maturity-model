import LastTab from './LastTab';
import styles from './TabContent.scss';
var fromEntries = require('object.fromentries');

export default class TabContent extends React.Component {

    constructor (props) {
        super(props)
        const values = fromEntries(this.props.data.map((item) => [ item.name, 0 ]));

        this.state = {
            values,
        }
    }

    handleChange = (key, value) => {
        const newValues = Object.assign(this.state.values, { [key]: Number(value) });

        this.setState({
            values: newValues,
        });
    }

    descriptiveText(value, texts) {
        let custLevel = "beginner";

        if (value > 25) {
            custLevel = "developing"
        } if (value > 50) {
            custLevel = "performing"
        } if (value > 75) {
            custLevel = "transforming"
        }
        return texts && texts[custLevel] || "This is default text";
    }

    doClick(index) {
        this.props.clickNext(index+1);
    }

    render() {
        let activeClass = this.props.activeId;

        if(activeClass === this.props.data.length) {

            return (
                <LastTab
                    values={this.state.values}
                    data={this.props.data}
                />
            )
        }

        else {
            return this.props.data.map((item, index) => {
                let itemValue = this.state.values[item.name];

                let BackgroundStyles = {
                    background: 'var(--main-bg-color)',
                    opacity: (itemValue / Math.pow(10, 2))
                };

                return (
                    <React.Fragment>
                        <style jsx>{styles}</style>

                        <article className={activeClass === index ? 'show' : ''} key={item}>
                            <span style={BackgroundStyles}></span>
                            <div>
                                <h4>{item.text}</h4>
                                <p>{this.descriptiveText(itemValue, item.descriptiveText)}</p>
                            </div>
                            <footer>
                                <input
                                    type="range"
                                    name="capSlider"
                                    id="capSlider"
                                    list="tickmarks"
                                    value={itemValue} 
                                    onChange={(event) => { this.handleChange(item.name, event.target.value) } }
                                />
                                <div className="confirm" onClick={this.doClick.bind(this, index)}>
                                    <label htmlFor="capSlider">{itemValue}</label>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z"></path></svg>
                                </div>
                            </footer>
                        </article>
                    </React.Fragment>
                )
            });
        }
    }
}