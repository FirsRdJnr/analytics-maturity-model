import styles from './LastTab.scss';

export default class LastTab extends React.Component {
    getDescriptiveText (name, data) {
        return data.filter(object => object.name === name).pop().text;
    }

    getLevelForScore(score) {
        let custLevel = "Beginner";

        if (score > 25) {
            custLevel = "Developing"
        } if (score > 50) {
            custLevel = "Performing"
        } if (score > 75) {
            custLevel = "Transforming"
        }

        return custLevel;
    }

    render() {

        
        return Object.entries(this.props.values).map(([item, value]) => {
            let BackgroundStyles = {
                background: 'var(--main-bg-color)',
                opacity: (value / Math.pow(10, 2))
            };

            return (
                <React.Fragment>
                    <style jsx>{styles}</style>

                    <article key={item}>
                        <span style={BackgroundStyles}></span>
                        <div>
                            <p>{this.getDescriptiveText(item, this.props.data)}</p>
                            <p>{value}</p>
                            <p>{this.getLevelForScore(value)}</p>
                        </div>
                    </article>

                </React.Fragment>
            )
        })
    }

}