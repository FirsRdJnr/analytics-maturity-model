import TabHeader from '../Layout//Tabs/TabHeader.js';
import TabContent from '../Layout/Tabs/TabContent.js';

import styles from './Tech.scss';

let data = [
    {
        name: 'Analytical Tools',
        text: 'How are you publishing and analysing your data? Are you in Excel Hell?Do all teams use the same set of tools to build their reporting and perform their analyses? Does anything fall between the gaps of your analytical tools?',
        descriptiveText: {
            beginner: 'All analysis and reporting is performed in excel with little or no automation',
            performing: 'Reports are produced using a variety of different tools, but not by a central function.',
            developing: 'Reporting is typically produced by a central team, with other reporting performed using a consistent small suite of products.',
            transforming: 'All business users have access to a small set of reporting and analytical tools, driving their work from a consistent, central data set ensuring consistency of approach and data.'
        }
    },
    {
        name: 'System Correlation',
        text: 'Is there a well-defined understanding of how data flows between your systems? Does Google Analytics match with other tracking tools?',
        descriptiveText: {
            beginner: 'Stuff just happens',
            performing: 'Aware of variance between systems and deficiencies of specific systems.',
            developing: 'Processes in place to monitor variance between systems, potentially with integration activity taking place.',
            transforming: 'Either: Systems all fully integrated and monitored for data quality or a central analytical data store has been created to handle variance.'
        }
    },
    {
        name: 'Appropriate tools',
        text: 'How suited to different capabilities within your team are your analytics tools? ',
        descriptiveText: {
            beginner: 'Only our analyst can provide insight - they are a bottle neck.',
            performing: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            developing: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.',
            transforming: ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        }
    },
    {
        name: 'Legacy',
        text: 'How much do legacy systems impede your analytical progress?',
        descriptiveText: {
            beginner: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            performing: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.',
            developing: ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            transforming: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }
    }
];

export default class Tech extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            activeTab: 0,
            data: data
        }
        this.changeTabOnClick = this.changeTabOnClick.bind(this);
    }

    changeTabOnClick(index) {
        this.setState({
            activeTab: index
        });
    }

    render () {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>

                <section>
                    <ul>
                        <TabHeader 
                            data={this.state.data}
                            click={this.changeTabOnClick}
                            activeId={this.state.activeTab}
                        />
                    </ul>

                    <TabContent
                        data={this.state.data}
                        activeId={this.state.activeTab}
                        clickNext={this.changeTabOnClick}
                    />

                </section>
            </React.Fragment>
        )
    }
}
