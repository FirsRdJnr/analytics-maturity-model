import TabHeader from '../Layout//Tabs/TabHeader.js';
import TabContent from '../Layout/Tabs/TabContent.js';

import styles from './Stratergy.scss';

let data = [
    {
        name: 'Key Performance Indicators',
        text: 'How well defined are your KPIs? Are these definitions governed centrally?',
        descriptiveText: {
            beginner: 'Each team and department has different measures of success',
            performing: 'Some KPIs are standardised across departments, with specific owners identified for each.',
            developing: 'The majority of KPIs are defined and accepted, and are regularly reported against.',
            transforming: 'KPIs are presented in a balanced scorecard, ensuring KPIs align to business objectives and strategy.'
        }
    },
    {
        name: 'Business Value Chain',
        text: 'How well defined is your business value chain? Identify primary business activities, sub activities for each primary activity, Support activities, Identify links between activities.',
        descriptiveText: {
            beginner: 'What’s a value chain?',
            performing: 'Understand key primary and supporting activities.',
            developing: 'Break down activities into sub-activities, with a view on value and cost of each.',
            transforming: 'Identify links and activities where margin/value can be improved or maximised.'
        }
    },
    {
        name: 'Scalability',
        text: 'Can your current data infrastructure support your growth plans?',
        descriptiveText: {
            beginner: 'Freegan knausgaard kitsch etsy. Hoodie taxidermy migas meh. Wolf salvia fanny pack humblebrag kogi wayfarers.',
            performing: 'you probably haven\'t heard of them aesthetic farm-to-table franzen ramps hexagon glossier seitan.',
            developing: 'Lumbersexual hashtag pop-up farm-to-table bicycle rights.',
            transforming: 'Brooklyn fixie hammock franzen pitchfork hoodie, bitters kombucha marfa tote bag.'
        }
    },
    {
        name: 'Decision making',
        text: 'What % of your decisions are made based on data and are measured & reported on afterwards?',
        descriptiveText: {
            beginner: 'Distillery gentrify yr, neutra tote bag af +1 organic. Gastropub adaptogen af chia.',
            performing: 'Kickstarter woke organic disrupt, tumblr forage tilde PBR&B skateboard +1 copper mug.',
            developing: 'Portland pabst messenger bag waistcoat iceland banjo listicle kickstarter shabby chic ennui chicharrones viral.',
            transforming: 'Mumblecore cred helvetica pitchfork taxidermy locavore food truck vice irony snackwave fingerstache.'
        }
    }
];

export default class Stratergy extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            activeTab: 0,
            data: data
        }
        this.changeTabOnClick = this.changeTabOnClick.bind(this);
    }

    changeTabOnClick(index) {
        this.setState({
            activeTab: index
        });
    }

    render () {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>

                <section>
                    <ul>
                        <TabHeader 
                            data={this.state.data}
                            click={this.changeTabOnClick}
                            activeId={this.state.activeTab}
                            
                        />
                    </ul>

                    <TabContent
                        data={this.state.data}
                        activeId={this.state.activeTab}
                        clickNext={this.changeTabOnClick}
                    />

                </section>
            </React.Fragment>
        )
    }
}
