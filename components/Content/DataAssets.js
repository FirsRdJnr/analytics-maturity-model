import styles from './DataAssets.scss';

export default class DataAssets extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: true,
            activeDataTypes: ["Marketing", "Operational", "Customer", "Manufacturing"]
        };
    }

    toggleSelected (dataType) {
        const exists = this.state.activeDataTypes.includes(dataType)
        let activeDataTypes = this.state.activeDataTypes;

        if (exists) {
            activeDataTypes = activeDataTypes.filter(adt => {
                return adt !== dataType // if it's different, include it
            })
        } else {
            activeDataTypes.push(dataType);
        }

        this.setState({
            activeDataTypes: activeDataTypes
        })
    };

    render() {
        const DataTypes = [
            {
                "type": "Marketing",
                "selected": "selected"
            },
            {
                "type": "Operational",
                "selected": "selected"
            },
            {
                "type": "Customer",
                "selected": "selected"
            },
            {
                "type": "Manufacturing",
                "selected": "selected"
            },
            {
                "type": "Analytic",
                "selected": "selected"
            },
            {
                "type": "Ecommerce",
                "selected": "selected"
            },
            {
                "type": "Logistics",
                "selected": "selected"
            },
            {
                "type": "Procurment",
                "selected": ""
            },
            {
                "type": "Licence",
                "selected": ""
            },
            {
                "type": "Service",
                "selected": ""
            },
        ];

        return (
            <React.Fragment>
            <style jsx>{styles}</style>

            <h2>3. What Data Assets do you hold</h2>
            <p>As a XXX business, we would expect you to have the following Data Assets. Please add any other sets that aren't highlighted below.</p>

            <section>
                <article>
                    <h4>Internal/Company Data</h4>
                    <ul>
                        <li className="selected">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 283.5"><path d="M141.7 28.3c-47.8 0-99.2 14.4-99.2 46.1V209c0 31.6 51.4 46.1 99.2 46.1 47.8 0 99.2-14.4 99.2-46.1V74.4c0-31.6-51.4-46.1-99.2-46.1zm85.1 180.8c0 17.6-38.1 31.9-85 31.9-47 0-85-14.3-85-31.9v-26.5c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.6v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V140c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.5v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V99.7c18.6 14.2 52.6 20.8 85 20.8s66.5-6.7 85-20.8V124zm-85.1-17.7c-47 0-85-14.3-85-31.9 0-17.6 38.1-31.9 85-31.9 47 0 85 14.3 85 31.9.1 17.6-38 31.9-85 31.9zm63.8 99.2c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-4 3.2-7.1 7.1-7.1z" /></svg>
                                IT
                            </span>
                            <input placeholder="Data owner" />
                        </li>
                        <li className="selected">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 283.5"><path d="M141.7 28.3c-47.8 0-99.2 14.4-99.2 46.1V209c0 31.6 51.4 46.1 99.2 46.1 47.8 0 99.2-14.4 99.2-46.1V74.4c0-31.6-51.4-46.1-99.2-46.1zm85.1 180.8c0 17.6-38.1 31.9-85 31.9-47 0-85-14.3-85-31.9v-26.5c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.6v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V140c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.5v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V99.7c18.6 14.2 52.6 20.8 85 20.8s66.5-6.7 85-20.8V124zm-85.1-17.7c-47 0-85-14.3-85-31.9 0-17.6 38.1-31.9 85-31.9 47 0 85 14.3 85 31.9.1 17.6-38 31.9-85 31.9zm63.8 99.2c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-4 3.2-7.1 7.1-7.1z" /></svg>
                                HR
                            </span>
                            <input placeholder="Data owner" />
                        </li>
                        <li className="selected">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 283.5"><path d="M141.7 28.3c-47.8 0-99.2 14.4-99.2 46.1V209c0 31.6 51.4 46.1 99.2 46.1 47.8 0 99.2-14.4 99.2-46.1V74.4c0-31.6-51.4-46.1-99.2-46.1zm85.1 180.8c0 17.6-38.1 31.9-85 31.9-47 0-85-14.3-85-31.9v-26.5c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.6v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V140c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.5v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V99.7c18.6 14.2 52.6 20.8 85 20.8s66.5-6.7 85-20.8V124zm-85.1-17.7c-47 0-85-14.3-85-31.9 0-17.6 38.1-31.9 85-31.9 47 0 85 14.3 85 31.9.1 17.6-38 31.9-85 31.9zm63.8 99.2c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-4 3.2-7.1 7.1-7.1z" /></svg>
                                Finance
                            </span>
                            <input placeholder="Data owner" />
                        </li>
                    </ul>
                </article>


                <article>
                    <h4>Operational Data</h4>
                    <ul>
                        {DataTypes.map(data => (
                            <li
                                key={data.type}
                                className={this.state.activeDataTypes.includes(data.type) ? 'selected' : ''}
                                onClick={() => this.toggleSelected(data.type)}
                            >
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 283.5"><path d="M141.7 28.3c-47.8 0-99.2 14.4-99.2 46.1V209c0 31.6 51.4 46.1 99.2 46.1 47.8 0 99.2-14.4 99.2-46.1V74.4c0-31.6-51.4-46.1-99.2-46.1zm85.1 180.8c0 17.6-38.1 31.9-85 31.9-47 0-85-14.3-85-31.9v-26.5c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.6v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V140c14.6 15.1 50 22.9 85 22.9s70.4-7.8 85-22.9v26.5zm0-42.5v.2c0 17.5-38.1 31.7-85 31.7-47 0-85-14.2-85-31.7V99.7c18.6 14.2 52.6 20.8 85 20.8s66.5-6.7 85-20.8V124zm-85.1-17.7c-47 0-85-14.3-85-31.9 0-17.6 38.1-31.9 85-31.9 47 0 85 14.3 85 31.9.1 17.6-38 31.9-85 31.9zm63.8 99.2c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-3.9 3.2-7.1 7.1-7.1zm0-42.5c3.9 0 7.1 3.2 7.1 7.1 0 3.9-3.2 7.1-7.1 7.1-3.9 0-7.1-3.2-7.1-7.1 0-4 3.2-7.1 7.1-7.1z" /></svg>
                                {data.type}
                            </span>
                            <input placeholder="Data owner" />
                        </li>
                        ))}
                    </ul>
                </article>

            </section>

            </React.Fragment>
        );
      }
}
