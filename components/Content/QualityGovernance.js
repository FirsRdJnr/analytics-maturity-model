import TabHeader from '../Layout//Tabs/TabHeader.js';
import TabContent from '../Layout/Tabs/TabContent.js';

import styles from './QualityGovernance.scss';

let data = [
    {
        name: 'Data Quality',
        text: 'How ‘good’ is your data quality? Do you have duplicate account records for customers? Does test data exist in your production systems?',
        descriptiveText: {
            beginner: 'What are these things of which you speak?',
            performing: 'Quality and definitions are handled in reports or analyses.',
            developing: 'Most definitions exist in a data warehouse or analytics repository, with responsibility for data quality residing with IT.',
            transforming: 'Systems and processes exist to ensure governance of definitions, with responsibility residing within functional areas.'
        }
    },
    {
        name: 'Board Involvement',
        text: 'How frequently is data discussed at board meetings?',
        descriptiveText: {
            beginner: 'Never',
            performing: 'Occasionally',
            developing: 'Frequently',
            transforming: 'Every time'
        }
    },
    {
        name: 'GDPR processes',
        text: 'If someone raised a subject access request how simple would it be to complete?',
        descriptiveText: {
            beginner: 'Only our analyst can provide insight - they are a bottle neck.',
            performing: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            developing: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.',
            transforming: ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        }
    },
    {
        name: 'Security',
        text: 'How confident are you in your data security?',
        descriptiveText: {
            beginner: 'It keeps me awake at night',
            performing: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            developing: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.',
            transforming: ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        }
    }
];

export default class QualityGovernance extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            activeTab: 0,
            data: data
        }
        this.changeTabOnClick = this.changeTabOnClick.bind(this);
    }

    changeTabOnClick(index) {
        this.setState({
            activeTab: index
        });
    }

    render () {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>

                <section>
                    <ul>
                        <TabHeader 
                            data={this.state.data}
                            click={this.changeTabOnClick}
                            activeId={this.state.activeTab}
                        />
                    </ul>

                    <TabContent
                        data={this.state.data}
                        activeId={this.state.activeTab}
                        clickNext={this.changeTabOnClick}
                    />

                </section>
            </React.Fragment>
        )
    }
}
