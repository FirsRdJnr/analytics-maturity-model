import TabHeader from '../Layout//Tabs/TabHeader.js';
import TabContent from '../Layout/Tabs/TabContent.js';

import styles from './People.scss';

let data = [
    {
        name: 'Business Unit Consensus',
        text: ' Do you have a formal team and systems for making sure key business entities are well managed and governed?',
        descriptiveText: {
            beginner: 'Different teams report on their view of a KPI.',
            performing: 'Known variances occur, but are accepted.',
            developing: 'Teams are actively working to resolve variance in reported numbers.',
            transforming: 'All teams are actively reporting the same base data, and KPIs are rarely mis-calculated.'
        }
    },
    {
        name: 'Analytical penetration (access to tools)',
        text: 'What percentage of your team have access to analytical tools to allow them to base their decisions on solid insight?',
        descriptiveText: {
            beginner: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            performing: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.',
            developing: ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            transforming: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }
    },
    {
        name: 'Actual usage',
        text: 'What percentage of your team actually use analytical tools in their decision making process?',
        descriptiveText: {
            beginner: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            performing: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.',
            developing: ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            transforming: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }
    },
    {
        name: 'Ownership/Stakeholders',
        text: 'Does your data have clearly defined and engaged stakeholders from the correct business domain?',
        descriptiveText: {
            beginner: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
            performing: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.',
            developing: ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            transforming: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }
    }
];

export default class People extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            activeTab: 0,
            data: data
        }
        this.changeTabOnClick = this.changeTabOnClick.bind(this);
    }

    changeTabOnClick(index) {
        this.setState({
            activeTab: index
        });
    }

    render () {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>

                <section>
                    <ul>
                        <TabHeader 
                            data={this.state.data}
                            click={this.changeTabOnClick}
                            activeId={this.state.activeTab}
                        />
                    </ul>

                    <TabContent
                        data={this.state.data}
                        activeId={this.state.activeTab}
                        clickNext={this.changeTabOnClick}
                    />

                </section>
            </React.Fragment>
        )
    }
}
