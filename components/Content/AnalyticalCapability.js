import TabHeader from '../Layout//Tabs/TabHeader.js';
import TabContent from '../Layout/Tabs/TabContent.js';

import styles from './AnalyticalCapability.scss';

let data = [
    {
        name: 'Analytical Capability',
        text: 'What can you do with the data you have? Descriptive Analytics -> Predictive Analytics -> Prescriptive Analytics.',
        descriptiveText: {
            beginner: ' Basic Descriptive Analytics.',
            performing: ' Advanced Descriptive Analytics.',
            developing: 'Mature Predictive Analytics & Visualisation.',
            transforming: 'Prescriptive Analytics.'
        }
    },
    {
        name: 'Time To Insight',
        text: 'How long does it take to wrangle data, build a report/analytical tool and then work out what to do?',
        descriptiveText: {
            beginner: 'See last week’s sales.',
            performing: 'See yesterday’s sales and build predictions for next week. Models take more than 1 month to build and train.',
            developing: 'See current sales and retrain models rapidly to react in less than 1 month.',
            transforming: 'Actively monitor real-time mix of scorecards and predictions, capable of retraining models in less than 24 hours.'
        }
    },
    {
        name: 'Time To Action',
        text: 'Once you know what to do, how long does it take you to then implement the insight? (think new propensity model).',
        descriptiveText: {
            beginner: 'Influencing business performance takes months.',
            performing: 'Influencing business performance takes less than 1 month.',
            developing: 'Influencing performance in less than 2 weeks.',
            transforming: 'Influence performance in near real-time.'
        }
    },
    {
        name: 'Business Definitions',
        text: 'What is a customer? What is an active customer? What is a sale? If you ask two business units what each means, will you get the same response?',
        descriptiveText: {
            beginner: 'Key entities like ‘Customer’ and ‘Sale’ are poorly defined and have multiple interpretations.',
            performing: 'A few key entities are well defined and accepted across the business.',
            developing: 'The majority of key entities are well defined and accepted across the business.',
            transforming: 'All key entities are defined and governed with a master data management system, and monitored for quality and errors.'
        }
    }
];

export default class AnalyticalCapability extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            activeTab: 0,
            data: data
        }
        this.changeTabOnClick = this.changeTabOnClick.bind(this);
    }

    changeTabOnClick(index) {
        this.setState({
            activeTab: index
        });
    }

    render () {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>

                <section>
                    <ul>
                        <TabHeader 
                            data={this.state.data}
                            click={this.changeTabOnClick}
                            activeId={this.state.activeTab}
                        />
                    </ul>

                    <TabContent
                        data={this.state.data}
                        activeId={this.state.activeTab}
                        clickNext={this.changeTabOnClick}
                    />

                </section>
            </React.Fragment>
        )
    }
}
