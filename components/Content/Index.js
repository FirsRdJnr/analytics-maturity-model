import BusinessStage from './BusinessStage';
import BusinessType from './BusinessType';
import DataAssets from './DataAssets';
import Stratergy from './Stratergy';
import AnalyticalCapability from './AnalyticalCapability';
import Tech from './Tech';
import People from './People';
import QualityGovernance from './QualityGovernance';

import styles from './Index.scss';

export default class Index extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            currentSection: 4, 
            clientAnswers: {}
        }
        this.advanceStage = this.nextStageButton.bind(this)
    }

    nextStageButton() {
        this.setState(prevState => {
            return {currentSection: prevState.currentSection + 1}
        })
    }

    render() {
        return (
            <React.Fragment>
                <style jsx>{styles}</style>

                <nav>
                    <ul>
                        <li className="complete">Maturity</li>
                        <li className="selected">Type</li>
                        <li>Data Assets</li>
                        <li>Strategy</li>
                        <li>Analytical Capability</li>
                        <li>Tech </li>
                        <li>People</li>
                        <li>Quality & Governance</li>
                    </ul>
                </nav>

                <section>
                    {this.state.currentSection === 1 && <BusinessStage />}
                    {this.state.currentSection === 2 && <BusinessType />}
                    {this.state.currentSection === 3 && <DataAssets />}
                    {this.state.currentSection === 4 && <Stratergy />}
                    {this.state.currentSection === 5 && <AnalyticalCapability />}
                    {this.state.currentSection === 6 && <Tech />}
                    {this.state.currentSection === 7 && <People />}
                    {this.state.currentSection === 8 && <QualityGovernance />}

                </section>
                <button
                    onClick={this.advanceStage}
                >
                    Next
                </button>


            </React.Fragment>
        )
    }
}
